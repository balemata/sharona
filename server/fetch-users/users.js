const createNewUser = async (name, password, phoneNumber) => {
  // 1. Save User Data on database
  // Fetch Just for testing //
  const localhost = Platform.OS === 'android' ? '10.0.2.2' : 'localhost';
  try {
    const response = await fetch(`http://${localhost}:3000/api/v1/users`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        name,
        password,
        phoneNumber,
      }),
    });

    const result = await response.json();
    return result;
    
  } catch (err) {
    console.log(err);
  }
};

const loginUser = async (phoneNumber, password) => {
  // 1. Check User Data on database
  // Fetch Just for testing //
  const localhost = Platform.OS === 'android' ? '10.0.2.2' : 'localhost';
  try {
    const response = await fetch(`http://${localhost}:3000/api/v1/login`, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        phoneNumber,
        password,
      }),
    });

    const result = await response.json();
    return result;
  } catch (err) {
    console.log(err);
  }
};

module.exports = { loginUser, createNewUser };
