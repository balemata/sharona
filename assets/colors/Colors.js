export default {
  primary: 'hsl(350, 100%, 90%)',
  secondary: 'hsl(350, 100%, 70%)',
  third: 'hsl(200,80%, 40%)',
  white: 'white',
  black: 'black',
  gray: 'gray',
  lightgrey: 'lightgrey'
};
