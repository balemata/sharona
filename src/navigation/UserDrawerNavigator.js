import React from "react";
import {Text} from 'react-native';
import { createDrawerNavigator } from '@react-navigation/drawer';

import { DrawerContent } from '../components/DrawerContent';

import HomePage from '../screens/HomePage';
import NavigateContact from '../screens/NavigateContact';
import BookAppointment from '../screens/BookAppointment';
import Gallery from '../screens/Gallery';
import AboutMe from '../screens/AboutMe';

const Drawer = createDrawerNavigator();

const UserDrawerNavigator = () => {
    return(
        <Drawer.Navigator
            screenOptions={screenOptionStyle}
            initialRouteName="HomePage"
            drawerPosition="right"
            drawerContent={(props) => <DrawerContent {...props} />}
        >
            <Drawer.Screen name="HomePage" component={HomePage} />
            <Drawer.Screen name="NavigateContact" component={NavigateContact} />
            <Drawer.Screen name="AboutMe" component={AboutMe} />
            <Drawer.Screen name="BookAppointment" component={BookAppointment} />
            <Drawer.Screen name="Gallery" component={Gallery} />
        </Drawer.Navigator>
    )
}

const screenOptionStyle = {
    headerShown: true,
    headerTitleAlign: 'center',
};

export default UserDrawerNavigator;