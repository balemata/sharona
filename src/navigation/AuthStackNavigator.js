import React from "react";
import { enableScreens } from 'react-native-screens';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';

import DrawerNavigator from './UserDrawerNavigator';
import EntryPage from '../screens/EntryPage';
import LoginPage from '../screens/LoginPage';
import SignUpPage from '../screens/SignUpPage';

enableScreens();
const Stack = createNativeStackNavigator();

const StackNavigator = () => {
    return(
        <Stack.Navigator 
            initialRouteName="EntryPage"
            screenOptions={screenOptionStyle}>
            
            <Stack.Screen name="EntryPage" component={EntryPage} />
            <Stack.Screen name="SignUpPage" component={SignUpPage} />
            <Stack.Screen name="LoginPage" component={LoginPage} />
            <Stack.Screen name="UserDrawerNavigator" component={DrawerNavigator} />
        </Stack.Navigator>
    )
}

const screenOptionStyle = {
    headerShown: false,
};

export default StackNavigator;




