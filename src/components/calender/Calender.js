import React from 'react';

import { Calendar, CalendarList, Agenda } from 'react-native-calendars';

const Calender = () => {
  return (
    <Calendar
      // Initially visible month. Default = Date()
      // current={'2020-06-21'}
      // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
      minDate={'2012-05-10'}
      // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
      maxDate={'3000-06-21'}
      // Handler which gets executed on day press. Default = undefined
      onDayPress={day => {}}
      // Handler which gets executed on day long press. Default = undefined
      onDayLongPress={day => {}}
      // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
      monthFormat={'yyyy MMM'}
      // Handler which gets executed when visible month changes in calendar. Default = undefined
      onMonthChange={month => {}}
      // Hide month navigation arrows. Default = false
      hideArrows={false}
      // Replace default arrows with custom ones (direction can be 'left' or 'right')
      // renderArrow={direction => <Arrow direction={direction} />}
      // Do not show days of other months in month page. Default = false
      hideExtraDays={true}
      // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
      // day from another month that is visible in calendar page. Default = false
      disableMonthChange={true}
      // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
      firstDay={0}
      // Hide day names. Default = false
      hideDayNames={false}
      // Show week numbers to the left. Default = false
      showWeekNumbers={false}
      // Handler which gets executed when press arrow icon left. It receive a callback can go back month
      onPressArrowLeft={substractMonth => substractMonth()}
      // Handler which gets executed when press arrow icon right. It receive a callback can go next month
      onPressArrowRight={addMonth => addMonth()}
      // Disable left arrow. Default = false
      disableArrowLeft={false}
      // Disable right arrow. Default = false
      disableArrowRight={false}
      // Disable all touch events for disabled days. can be override with disableTouchEvent in markedDates
      disableAllTouchEventsForDisabledDays={true}
      /** Replace default month and year title with custom one. the function receive a date as parameter. **/
      renderHeader={date => {
        <Text>Matan</Text>;
      }}
      theme={{
        backgroundColor: Colors.primary,
        calendarBackground: Colors.primary,
        textSectionTitleColor: 'black',
        textSectionTitleDisabledColor: '#d9e1e8',
        selectedDayBackgroundColor: 'green',
        selectedDayTextColor: '#ffffff',
        todayTextColor: '#00adf5',
        dayTextColor: '#2d4150',
        textDisabledColor: '#d9e1e8',
        dotColor: '#00adf5',
        selectedDotColor: '#ffffff',
        disabledArrowColor: '#d9e1e8',
        arrowColor: 'blue',
        monthTextColor: '#00adf5',
        textDayFontWeight: 'bold',
        textMonthFontWeight: 'bold',
        textDayHeaderFontWeight: 'bold',
        textDayFontSize: 16,
        textMonthFontSize: 16,
        textDayHeaderFontSize: 16
      }}
    />
  );
};

export default Calender;
