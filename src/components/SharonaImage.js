import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

const ImageSrc = require('../../assets/images/sharonaImage.gif');
// const ImageSrc = require('../../assets/images/sharona-image.jpg');

class SharonaImage extends Component {
  render() {
    return <Image style={styles.sharonaImage} source={ImageSrc} />;
  }
}

const styles = StyleSheet.create({
  sharonaImage: {
    width: '100%',
    height: hp('15%')
  }
});

export default SharonaImage;
