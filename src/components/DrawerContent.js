import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { DrawerItem, DrawerContentScrollView } from '@react-navigation/drawer';
import ProfileInfo from './ProfileInfo';

import Icon from 'react-native-vector-icons/FontAwesome5';

export function DrawerContent(props) {
  return (
    <View
      style={{
        flex: 1,
      }}
    >
      <DrawerContentScrollView {...props}>
        <View style={styles.picContainer}>
          <ProfileInfo />
        </View>
        <View style={styles.container}>
          <DrawerItem
            labelStyle={styles.lableStyle}
            icon={({ color, size }) => (
              <Icon color={color} size={size} name={'home'} />
            )}
            label="ראשי"
            onPress={() => {
              props.navigation.navigate('HomePage');
            }}
          />
          <DrawerItem
            labelStyle={styles.lableStyle}
            label="קצת עליי"
            icon={({ color, size }) => (
              <Icon color={color} size={size} name={'female'} />
            )}
            onPress={() => {
              props.navigation.navigate('AboutMe');
            }}
          />
          <DrawerItem
            labelStyle={styles.lableStyle}
            icon={({ focused, color, size }) => (
              <Icon color={color} size={size} name={'calendar-alt'} />
            )}
            label="הזמנת תור"
            onPress={() => {
              props.navigation.navigate('BookAppointment');
            }}
          />
          <DrawerItem
            labelStyle={styles.lableStyle}
            icon={({ focused, color, size }) => (
              <Icon
                color={color}
                size={size}
                name={'calendar-times'}
              />
            )}
            label="ביטול תור"
            onPress={() => {}}
          />
          <DrawerItem
            labelStyle={styles.lableStyle}
            icon={({ focused, color, size }) => (
              <Icon color={color} size={size} name={'paint-brush'} />
            )}
            label="גלרית עבודות"
            onPress={() => {
              props.navigation.navigate('Gallery');
            }}
          />
          <DrawerItem
            labelStyle={styles.lableStyle}
            icon={({ focused, color, size }) => (
              <Icon color={color} size={size} name={'map-marker-alt'} />
            )}
            label="איך מגיעים"
            onPress={() => {
              props.navigation.navigate('NavigateContact');
            }}
          />
        </View>
        <View style={styles.bottomDrawerSection}>
          <DrawerItem
            labelStyle={styles.lableStyle}
            icon={({ focused, color, size }) => (
              <Icon color={color} size={size} name={'sign-out-alt'} />
            )}
            label="התנתקי"
            onPress={() => {}}
          />
        </View>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 20,
  },
  picContainer: {
    // borderWidth: 3,
    // borderColor: 'red',
    height: 220,
  },
  container: {
    // borderWidth: 3,
    // borderColor: 'red',
    height: 350,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
  },
  lableStyle: {
    color: '#888',
    fontWeight: 'bold',
    textAlign: 'left',
  },
  row: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  section: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 15,
  },
  paragraph: {
    fontWeight: 'bold',
    marginRight: 3,
  },
  drawerSection: {
    marginTop: 15,
  },
  bottomDrawerSection: {
    borderTopColor: '#888',
    borderTopWidth: 1,
    // borderWidth: 3,
    // borderColor: 'green',

    justifyContent: 'flex-end',
  },
  preference: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 12,
    paddingHorizontal: 16,
  },
});
