import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from 'react-native';

const Schedule = (props) => {
  return (
    <View style={styles.screen}>
      <Text style={styles.title}>קביעת תור</Text>
      <ScrollView contentContainerStyle={{}}>
        <View style={styles.main}>
          <Text style={styles.time}>10:00-11:00</Text>
          <TouchableOpacity style={styles.openButton}>
            <Text>קבע תור</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.main}>
          <Text style={styles.time}>11:00-12:00</Text>
          <TouchableOpacity style={styles.openButton}>
            <Text>קבע תור</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.main}>
          <Text style={styles.time}>13:00-14:00</Text>
          <TouchableOpacity style={styles.closeButton}>
            <Text>תפוס</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.main}>
          <Text style={styles.time}>14:00-15:00</Text>
          <TouchableOpacity style={styles.closeButton}>
            <Text>תפוס</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.main}>
          <Text style={styles.time}>15:00-16:00</Text>
          <TouchableOpacity style={styles.closeButton}>
            <Text>תפוס</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.main}>
          <Text style={styles.time}>16:00-17:00</Text>
          <TouchableOpacity style={styles.openButton}>
            <Text>קבע תור</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.main}>
          <Text style={styles.time}>17:00-18:00</Text>
          <TouchableOpacity style={styles.openButton}>
            <Text>קבע תור</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  title: {
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  main: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginTop: 10,
    paddingHorizontal: 20,
  },
  openButton: {
    width: '20%',
    height: '100%',
    borderColor: 'green',
    borderWidth: 2,
    alignItems: 'center',
    borderRadius: 30,
  },
  closeButton: {
    width: '20%',
    height: '100%',
    backgroundColor: 'red',
    borderWidth: 2,
    alignItems: 'center',
    borderRadius: 30,
    borderColor: 'red',
  },
  time: {
    fontWeight: 'bold',
  },
});

export default Schedule;
