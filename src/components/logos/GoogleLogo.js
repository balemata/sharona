import React from 'react';
import { Image, StyleSheet, Platform } from 'react-native';

const googleLogo = require('../../../assets/images/google-logo.png');

function GoogleLogo() {
  return <Image style={styles.image} source={googleLogo} />;
}

const styles = StyleSheet.create({
  image: {
    width: 32,
    height: 30
  }
});

export default GoogleLogo;
