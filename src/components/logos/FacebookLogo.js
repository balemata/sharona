import React from 'react';
import { Image, StyleSheet, Platform } from 'react-native';

const facebookLogo = require('../../../assets/images/facebook-logo.png');

function FacebookLogo() {
  return <Image style={styles.image} source={facebookLogo} />;
}

const styles = StyleSheet.create({
  image: {
    width: 30,
    height: 30
  }
});

export default FacebookLogo;
