import React from 'react';
import { View, StyleSheet } from 'react-native';
import Slideshow from 'react-native-image-slider-show';

const MyPicsGallery = () => {
  return (
    <View style={styles.container}>
      <Slideshow
        dataSource={[
          { url: 'http://placeimg.com/640/480/any' },
          { url: 'http://placeimg.com/640/480/any' },
          { url: 'http://placeimg.com/640/480/any' }
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderColor: 'red',
    borderRadius: 50
  }
});

export default MyPicsGallery;
