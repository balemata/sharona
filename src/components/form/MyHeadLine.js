import React from "react";

import { View, Text, StyleSheet, Linking } from "react-native";

import Icon from "react-native-vector-icons/Zocial";

const MyHeadLine = () => {
    return (
        <View style={styles.textContainer}>
            <Text style={styles.text}>בניית ציפורניים בשיטות אקריל וג'ל</Text>
            <Text style={styles.text}>מניקור רוסי ולק ג'ל - O.P.I</Text>
            <Text style={styles.text}>עיצוב ושיקום גבות</Text>
            <Text style={styles.text}>איפור ערב וכלות</Text>
            <Text style={styles.city}>נתניה</Text>
            <Text onPress={() => alert("Call to this number")} style={styles.phoneNumber}>
                052-6665651
            </Text>
            <Icon name="call" color={"green"} size={35} onPress={() => alert("Call to the number")} />
        </View>
    );
};

const styles = StyleSheet.create({
    textContainer: {
        alignItems: "center",
        marginTop: "8%",
    },
    text: {
        fontSize: 20,
    },
    phoneNumber: {
        fontSize: 22,
        marginTop: 10,
        fontWeight: "bold",
    },
    city: {
        fontSize: 23,
        marginTop: 10,
    },
});

export default MyHeadLine;
