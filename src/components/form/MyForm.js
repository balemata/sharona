import React, { useRef, useReducer, useState } from 'react';
import { View, Text, TextInput, StyleSheet } from 'react-native';

import Colors from '../../../assets/colors/Colors';
import ConnectButton from './ConnectButton';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import * as validate from '../../validation/InputsValidation';

const MyForm = props => {
  const [name,setName] = useState('');
  const [nameIsValid,setNameIsValid] = useState(true);
  const [password,setPassword] = useState('');
  const [passIsValid,setPassIsValid] = useState(true);
  const [phoneNumber,setPhoneNumber] = useState('');
  const [phoneIsValid,setPhoneIsValid] = useState(true);

  const ref_input_2 = useRef();
  const ref_input_3 = useRef();
  const ref_input_4 = useRef();

  textChangeHandler = (inputId, text) => {
    if(inputId === 'name') {
      setName(text);
      setNameIsValid(true);
    }
    if(inputId === 'password') {
      setPassword(text);
      setPassIsValid(true);
    }
    if(inputId === 'phoneNumber') {
      setPhoneNumber(text);
      setPhoneIsValid(true);
    }
  };

  const onSubmit = () => {
    // Form Validation
    let nameValid = validate.validateName(name)
    let passValid = validate.validatePassword(password)
    let phoneValid = validate.validatePhoneNumber(phoneNumber)

    // For Sign Up Page
    if(nameValid.valid && passValid.valid && phoneValid.valid){
      if (props.newUser) {
        props.onSignUpSubmit(
          name,
          password,
          phoneNumber
        );
      } else {
        // For Login Page
        props.onLoginSubmit(
          phoneNumber,
          password
        );
      }
    } else {
        if(!nameValid.valid) setNameIsValid(false);
        if(!passValid.valid) setPassIsValid(false);
        if(!phoneValid.valid) setPhoneIsValid(false);
        alert('הטופס חייב להיות מלא ותקין.')
    }
    
  };

  return (
    <View style={styles(props).container}>
      <TextInput
        name={props.newUser ? 'name' : 'phoneNumber'}
        style={styles(props).inputs}
        placeholder={props.newUser ? 'שם' : 'מספר טלפון'}
        returnKeyType="default"
        onSubmitEditing={() => {
          ref_input_2.current.focus();
        }}
        onChangeText={textChangeHandler.bind(
          this,
          props.newUser ? 'name' : 'phoneNumber'
        )}
      />

      {!nameIsValid && props.newUser && <Text>חייב להיות מלא</Text>}
      {!phoneIsValid && !props.newUser && <Text>טלפון חייב להיות תקין</Text>}

      <TextInput
        name='password'
        style={styles(props).inputs}
        placeholder="סיסמא"
        secureTextEntry={true}
        returnKeyType="default"
        ref={ref_input_2}
        onSubmitEditing={() => {
          props.newUser ? ref_input_3.current.focus() : null;
        }}
        onChangeText={textChangeHandler.bind(this, 'password')}
      />

      {!passIsValid && <Text>סיסמא חייבת להכיל מינימום 4 תווים</Text>}

      {props.newUser ? (
        <TextInput
          name='phoneNumber'
          style={styles(props).inputs}
          placeholder="מספר טלפון"
          keyboardType="number-pad"
          ref={ref_input_4}
          onChangeText={textChangeHandler.bind(this, 'phoneNumber')}
        />
      ) : null}

      {!phoneIsValid && props.newUser && <Text>טלפון חייב להיות תקין</Text>}

      <View>
        <ConnectButton
          title={props.newUser ? 'הירשם' : 'התחבר'}
          button={'connect'}
          color={Colors.secondary}
          marginTop={wp('10%')}
          onSubmit={onSubmit}
          onLoginSubmit={props.onLoginSubmit}
          signUpIsLoading={props.signUpIsLoading}
          loginIsLoading={props.loginIsLoading}
        />

        <ConnectButton
          title={props.newUser ? 'אני משתמש קיים' : 'אני משתמש חדש'}
          button={'newuser'}
          color={Colors.third}
          marginTop={hp('2%')}
          onSubmit={props.changeUserStatus}
          signUpIsLoading={props.signUpIsLoading}
          loginIsLoading={props.loginIsLoading}
        />
      </View>
    </View>
  );
};

export const styles = props =>
  StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
    },
    inputs: {
      textAlign: 'center',
      marginTop: hp('2%'),
      width: wp('50%'),
      borderWidth: hp('0.25%'),
      height: wp('11%'),
      borderRadius: 20,
      fontWeight: 'bold',
      fontSize: hp('1.9%'),

      // Shadow
      // IOS shadow
      shadowColor: 'black',
      shadowOffset: {
        width: 0,
        height: 1
      },
      shadowOpacity: 0.5,
      shadowRadius: 3,

      // Android shadow
      elevation: 10
      //
    }
  });

export default MyForm;
