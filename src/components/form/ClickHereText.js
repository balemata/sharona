import React from 'react';
import { Text, StyleSheet, TouchableOpacity } from 'react-native';

const ClickHereText = props => {
  return (
    <Text style={styles.text1}>
      If you don't have Facebook or Google account click{' '}
      <Text onPress={props.onHerePress} style={styles.here}>
        here
      </Text>
    </Text>
  );
};

const styles = StyleSheet.create({
  text1: {
    // marginTop: '5%',
    fontSize: 14
  },
  here: {
    textDecorationLine: 'underline'
  }
});

export default ClickHereText;
