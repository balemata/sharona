import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import Colors from '../../../../assets/colors/Colors';

import {
  GoogleSignin,
  statusCodes
} from '@react-native-community/google-signin';

import LoadingSpinner from '../../../components/LoadingSpinner';

const iosApiClientID =
  '747288998819-lcnjli471pg4l98c4p1q5in62o9c6sbs.apps.googleusercontent.com';

const googleClientID =
  '747288998819-asb2f0ukikb1qja7q2u4igmksajv4fbo.apps.googleusercontent.com';

const GoogleConnectButton = props => {
  // Google Connection
  const _signIn = async () => {
    GoogleSignin.configure({
      iosClientId: iosApiClientID,
      webClientId: googleClientID
    });
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      if (userInfo) {
        props.onGoogleLoginSuccess(
          userInfo.user.name,
          userInfo.user.email,
          userInfo.idToken,
          userInfo.user.photo
        );
      }
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
    }
  };

  if (props.googleIsLoading) {
    return (
      <TouchableOpacity style={styles.connectButtonCon}>
        <LoadingSpinner />
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity onPress={_signIn} style={styles.connectButtonCon}>
        <Text style={styles.text}>{`${props.title} דרך גוגל`}</Text>
      </TouchableOpacity>
    );
  }
};

const styles = StyleSheet.create({
  connectButtonCon: {
    backgroundColor: 'hsl(0, 100%, 70%)',
    marginTop: hp('2%'),
    width: wp('45%'),
    height: wp('10%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1

    // borderColor: 'black',
    // borderWidth: 1,
  },
  text: {
    fontSize: wp('4%'),
    color: Colors.white
  }
});

export default GoogleConnectButton;
