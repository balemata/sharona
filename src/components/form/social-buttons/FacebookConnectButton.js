import React, { Component } from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import Colors from '../../../../assets/colors/Colors';

import {
  LoginManager,
  GraphRequest,
  GraphRequestManager
} from 'react-native-fbsdk';

import LoadingSpinner from '../../../components/LoadingSpinner';

const FacebookConnectButton = props => {
  const _handleFacebookLogin = () => {
    LoginManager.logInWithPermissions([
      'public_profile',
      'email'
      // 'user_birthday'
    ]).then(result => {
      if (result.isCancelled) {
        console.log('Canceled.');
      } else {
        const infoRequest = new GraphRequest(
          '/me?fields=email,name,picture.type(large)',
          null,
          _get_Response_Info
        );
        new GraphRequestManager().addRequest(infoRequest).start();
      }
    });
  };

  // Facebook Conecction
  const _get_Response_Info = (error, result) => {
    if (error) {
      alert('קיימת שגיאה, אנא בדוק את החיבור לאינטרנט ונסה שנית.');
    } else if (!result.isCancelled) {
      props.onFacebookLoginSuccess(
        result.name,
        result.email,
        result.id,
        result.picture.data.url
      );
    }
  };

  if (props.facebookIsLoading) {
    return (
      <TouchableOpacity style={styles.connectButtonCon}>
        <LoadingSpinner />
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
        style={styles.connectButtonCon}
        onPress={_handleFacebookLogin}
      >
        <Text style={styles.text}>{`${props.title} דרך פייסבוק`}</Text>
      </TouchableOpacity>
    );
  }
};

const styles = StyleSheet.create({
  connectButtonCon: {
    backgroundColor: '#4267B2',
    marginTop: hp('2%'),
    width: wp('45%'),
    height: wp('10%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1

    // borderColor: 'black',
    // borderWidth: 1,
  },
  text: {
    fontSize: wp('4%'),
    color: Colors.white
  }
});

export default FacebookConnectButton;
