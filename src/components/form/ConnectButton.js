import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

import {
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';

import Colors from '../../../assets/colors/Colors';

const ConnectButton = props => {
    return (
      <TouchableOpacity
        onPress={props.onSubmit}
        style={{...styles.buttonCon, backgroundColor: props.color, marginTop: props.marginTop}}
      >
        <Text style={styles.buttonTitle}>{props.title}</Text>
      </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
  buttonCon: {
    width: wp('50%'),
    height: wp('10%'),
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1.5,

    // Shadow
    // IOS shadow
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.5,
    shadowRadius: 3,

    // Android shadow
    elevation: 10
    //
  },
  buttonTitle: {
    fontSize: wp('4.5%'),
    fontWeight: 'bold',
    color: Colors.white
  }
});

export default ConnectButton;
