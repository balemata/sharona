import React from 'react';
import { Text } from 'react-native';

import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

const PoweredByText = () => {
  return <Text style={{ fontSize: hp('1.7%') }}>Powered by MT</Text>;
};

export default PoweredByText;
