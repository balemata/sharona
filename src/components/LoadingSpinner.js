import React from 'react';

import { View, ActivityIndicator } from 'react-native';

const LoadingSpinner = () => {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <ActivityIndicator size="small" color="white" />
    </View>
  );
};

export default LoadingSpinner;
