import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

import { heightPercentageToDP as hp,
  widthPercentageToDP as wp, } from 'react-native-responsive-screen';
import Colors from '../../assets/colors/Colors';

const EntryPageButton = props => {
  buttonClicked = () => {
    props.createAccountButtonClicked();
  };

  return (
    <TouchableOpacity onPress={() => buttonClicked()} style={styles.button}>
      <Text style={{ color: 'white', fontSize: 20, fontWeight: 'bold' }}>
        {props.title}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.secondary,
    width: wp('50%'),
    height: hp('6%'),
    borderRadius: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: 'black',

    // Shadow
    // IOS shadow
    shadowColor: Colors.black,
    shadowOffset: {
      width: 1,
      height: -1
    },
    shadowOpacity: 0.42,
    shadowRadius: 8,

    // Android shadow
    elevation: 10
    //
  }
});

export default EntryPageButton;
