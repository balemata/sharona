import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Dimensions
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';

const noPicture = require('../../assets/images/user-image.png');

const ProfileInfo = () => {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [email, setEmail] = useState('');
  const [idToken, setIdToken] = useState('');
  const [imageUrl, setImageUrl] = useState('');

  useEffect(() => {
    const action = async () => {
      const response = await AsyncStorage.multiGet([
        'name',
        'password',
        'email',
        'idToken',
        'imageUrl'
      ]);

      setName(response[0][1]);
      setPassword(response[1][1]);
      setEmail(response[2][1]);
      setIdToken(response[3][1]);
      setImageUrl(response[4][1]);
    };
    action();
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.helloText}>{name}</Text>
      <TouchableOpacity style={styles.imageConatiner}>
        {imageUrl ? (
          <Image source={{ uri: imageUrl }} style={styles.profileImage} />
        ) : (
          <Image source={noPicture} style={styles.profileImage} />
        )}
      </TouchableOpacity>
      <Text style={styles.nextAppointment}>שלום אורחת, הפגישה הבאה</Text>
      <Text style={styles.date}>1.1.2021, 13:30</Text>
    </View>
  );
};

// Android Dimensions:
// Width: 411.42857142857144
// Height: 683.4285714285714

// IOS Dimensions:
// Width: 414
// Height: 896

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },
  profileImage: {
    width: Dimensions.get('window').width < 414 ? 60 : 75,
    height: Dimensions.get('window').height < 896 ? 60 : 75,
    borderRadius: 50,
    marginTop: Platform.OS === 'android' ? '3%' : '5%',
    borderColor: 'black',
    borderWidth: 1
  },
  imageConatiner: {
    // Shadow
    // IOS shadow
    shadowColor: 'black',
    shadowOffset: {
      width: 1,
      height: -2
    },
    shadowOpacity: 0.42,
    shadowRadius: 8,

    // Android shadow
    elevation: 10
    //
  },
  helloText: {
    fontSize: Dimensions.get('window').width < 414 ? 18 : 26,
    fontWeight: 'bold'
  },
  nextAppointment: {
    fontSize: Dimensions.get('window').width < 414 ? 18 : 26,
    marginTop: Dimensions.get('window').width < 414 ? '4%' : '6%'
  },
  date: {
    fontSize: Dimensions.get('window').width < 414 ? 18 : 26,
    marginTop: Dimensions.get('window').width < 414 ? '3%' : '6%',
    fontWeight: 'bold'
  }
});

export default ProfileInfo;
