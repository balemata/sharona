import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ProfileInfo from '../components/ProfileInfo';

export default class HomePage extends React.Component {
  constructor(props) {
    super(props);
  }
  onButtonClicked = (title) => {
    if (title === 'Make An Appointment') {
      this.props.navigation.navigate('MakeAppointment');
    }
  };
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.Client}>
            <ProfileInfo />
          </View>
          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri:
                  'https://main-cdn.grabone.co.nz/goimage/fullsize/32152b240f438384ebd07d102853c7b50934ebcc.jpg',
              }}
              style={styles.imageAboutContainer}
            />

            <TouchableOpacity style={styles.btnAboutMe}>
              <Text>קצת עליי</Text>
            </TouchableOpacity>
          </View>

          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri:
                  'https://assets.rebelmouse.io/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpbWFnZSI6Imh0dHBzOi8vYXNzZXRzLnJibC5tcy82NDcyMTMwL29yaWdpbi5qcGciLCJleHBpcmVzX2F0IjoxNjE0NDYzOTI3fQ.iAFB1f8WjYM-i5fGABMhpGHKNryjgaouoOQU4Bj9WEk/img.jpg?width=980',
              }}
              style={styles.imageAboutContainer}
            />
            <View
              style={{
                width: '100%',
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}
            >
              <TouchableOpacity style={{ ...styles.btnAboutMe, width: '35%' }}>
                <Text>כתובת ויצירת קשר</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ ...styles.btnAboutMe, width: '35%' }}>
                <Text>ניווט בעזרת Waze</Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.containerFollowUs}>
            <View>
              <Text style={styles.followUsText}>עקבו אחריי</Text>
            </View>
            <View style={styles.followUs}>
              <Icon onPress={() => {}} size={26} name={'facebook'} />
              <Icon onPress={() => {}} size={26} name={'instagram'} />
              <Icon onPress={() => {}} size={26} name={'whatsapp'} />
            </View>
          </View>
        </View>
        <Text style={{ textAlign: 'center' }}>
          {' '}
          Cמערכות לניהול עסקים | M{`&`}T
        </Text>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: '100%',
    // borderColor: 'blue',
    // borderWidth: 3,
    // justifyContent: 'flex-end',
  },
  btnAboutMe: {
    alignItems: 'center',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: '#888',
    width: '20%',
  },
  imageAboutContainer: {
    width: '100%',
    height: '90%',
    borderRadius: 30,
  },
  aboutMeContainer: {
    // borderColor: 'yellow',
    // borderWidth: 3,
    width: '100%',
    height: 200,
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  Client: {
    // borderColor: 'yellow',
    // borderWidth: 3,
    width: '100%',
    height: 200,
  },
  containerFollowUs: {
    width: '100%',
    alignItems: 'center',
    marginBottom: '20%',

    // borderColor: 'yellow',
    // borderWidth: 3,
  },

  followUsText: {
    fontSize: 22,
    fontWeight: 'bold',
    marginTop: 30,
    marginBottom: 20,
  },
  followUs: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
});
