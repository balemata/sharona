import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default class Gallery extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={styles.screen}>
        <ScrollView>
          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri:
                  'https://media1.popsugar-assets.com/files/thumbor/xaHSsP5eU8TUnRihhZVcBihB3wM/fit-in/2048xorig/filters:format_auto-!!-:strip_icc-!!-/2018/08/27/022/n/1922153/d5e9d52c5b848a1c6ba239.11985245_/i/Best--Home-Gel-Nails-Kit.jpg',
              }}
              style={styles.imageAboutContainer}
            />
          </View>
          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri:
                  'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOzQ+PTo0OThBSV1PQUVYRjg5UW5SWGBjaGloP05yenFleV1maGQBERISGBUYLxoaL2RDOUNkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZGRkZP/AABEIAKgBKwMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAADAAIEBQYBB//EADcQAAEDAwIFAgUDAwIHAAAAAAEAAgMEERIFISIxQVFhBhMUIzJCcYGRoTNSYsHhFSREctHw8f/EABgBAAMBAQAAAAAAAAAAAAAAAAECAwAE/8QAHxEBAQEBAQADAQEBAQAAAAAAAAERAiEDEjFBYVEy/9oADAMBAAIRAxEAPwDGu+pdDinOamEKUrH5FLJNASKzCNcigqOEVjklgC2T42F7gGAkk2AHMobTlstj6f0f4doqakfNI4Wn7VpDSaHpvpcPiEla8gkfQ02spsnpmhc2zMge4cVcgpBaxWcyMpU+l5GXMEt/DlSVNNNSyYTMLD/BXo5aoGpUEdXAWPG/Q9QhZgXiX8YEcScyIyyMjZuXGwRKinfTzvieNwVP0GAP1KO/28SWJZ7jV6VQx6fRMiYOO13O6kqYCmgpwTuiTJhxK5ZIBdWYN7VQa7pnut96McbeflaMhAlZk0hCjmzGAwSxxVnqtCaeQyMHATv4TdFohXVtni8bN3eUJ6j9bLjmnaRU11n/ANOL+89Vo6bQaGGOz4xIepfurONoZGGMFgOi6U2eHkikq/TtDM04MMR7sNln9Q0SporvHzY+7RuFuChSNy2slo3iV5uWobmLbV+kU1S0nARydHN2WSqIJKacxSCxH7EIbiXXH1QXxqO4FqsS3JNdCHKvPf8A0iuJXFMfTKK9hYqSysZddSSTMSSS4sxEpXXLrqzJRCbZIFPBUPYxpCYQjWRKaklq5RFCy5P7BGVoigJwK19F6TibHepeXvPQbAJ1T6Up8fkkg/lC2HnFqu9KUIqaszSC7IeQ7uW1CqNAoTp9IYn/AFl9ye6t2ofqnMz9EBTwE1qeCiJALjgugpFCjGe13TPd+bGOMc/Kr/T/AAVz77ENWrlaHNsqCrpvhq0VMY8PASXxvrLfsvmHJEAUOklD2hTWlPLo9TDgF0BcBXQUQIhDcERNKWjFdXU4mjLCNiLKHodL8E2UHmX7FW0gUNjsJyxJuU+S+rNqchsPCn3VdTIhCeEW6a5LRRZAqXV9PFbHdm0rd2nur2QKDKcZAkGzZlYosfFIWPFiDYgroC0mqaYyrj9yPaUD91l5C+GQseLEGxBWc3XP1ohao08V0ZsuS48puerKCrcMXLiPM1AK6JdgOJJJFMzhXVwrqwjEpzSmlOYOilWTaGilrpRHGPyegW30nTItPgsBd55nqUHRKNlJRRi3GRdxVq0JVeeZPT2jJde3hSaV0lLVIhtd8wqS0qJP8qQP6HmisetzR6n9SQU4FCackQIlPC6UwFOBWY1wUGtjyjJtyVgUGVqSw/KmpZhFKGX2PJXMb8mrM6iDSVv+DuJpVxQVAliBv0Wlweps1aApXQ2uXbp9Jh4K6UMFOBWYx4VdWjBzJR0Nj+FZOF1FqI843sPUWU6fkWnflGCpAKqdLmLoyw82nEqyDk8/A6mCEpjiuApOPlalhhKgV1mqW9yrdQJ9sv7c0h5BIpeihappEddGXs4JQNj3XIZVMbNi1H8DrmWYw80MlNKY5BZ4/lIO4Vq9X04ahTe4wfNbuD3WPeHxOLHggjYgoya5u+PrXJFGcivcgkq/P4RxJIlcJTsS6mkpXWFIKlaez3a6Bnd6iq09OsB1aO/QXCkM9rdxDGMDwjAqPmiB6R0SC5JF6FkuByS00jlQ3OMhRKeboeYNipjjkqepd8PVh/2O2PhCfps2LqN+SkAqsppslPY7JUidmCgpFya4phdita0gxcmOch5h3P8AQJhk4UlppEXU6NlbAYybP5sd2KpNIqnwzvppNntNrLRHiaVlNcYaTUmVDNg8X/UIT07XxSZNRSVT6XWCaJhB5hWrXdU8JZ6IPKcP1Qskg4u/8lYMFJQpF1zg3mU0nh3S0YpmyiHV3x/3tyt/CuWv4bqi1qEskirWc4jZ/lpUyCpyjB8IS4pZsT3SoTqjoFGc8u6oZK1ulkiV7gQpW5xkdwhhyK05IGzxQiX2nEE7g2UmlMlTIABt1K7U6cH13uX4HblvlWMRZDHZgAsmBLjayKOxVB6h0plTEZoBaVvbqrB9SXOsN0aKGR7dxzWlyk652evNXktdY8xsh5LS+o/T8rJH1VMLsO7mjmssSunmyxy2ZTi5cLk0lNumA8lIO2TF0clmegs9I02O73E97ocegHT62OaF5IBsQVp43ZNQ6j6Vz7XROYhPdinMeoz35ORGlLVEnJIFBBXHyhjd0MMkAqBqcWcZQpNS54ML7cy0XCGzUY5uAnfsditlhftNwLTqo5GJ542/yr2KXhWXq2GGUTR9Df8AKuqeYPiY8ciLhMFWgkTHyqJ7p6JjpeiS0ZB3ypgeUFr8v9U7JCnSWvUTVaIV1IY9hIOJh7FED0QOW/GxlNGrH01SaeS44iLHoVsopQ6NZfXqIMqWVkYsHWD7dHdCrPS6v3oGAnfkU/8Apf3xcA5bn9E18py2/wBkB0yaH36JdbBxJj5KcH5KMCiMKAnTMEsZa8XBFiD1VJZ9FL8O8kgbsJ6hXhdwqFqNP8RBwf1G8TCiM8CbJwrrpFWwVGTUfPJYUkPRo3qAHhvVFjmGXNZkybhjv23URrnzOsxTAcmocbmQ7dkf4VKpqZkTbnmpJkDVWOrPKfAJah22w7rfn415/tTXyxubY7rPax6cpqxjpKYCOXnt1Wijo2N3eblOcxjOQTS2VPqc15FNE+GV0cgs9psQhrS+sqRsVZHOwf1BZ35WbXRLsc1mXHE4ck1OHJEHq9BN7tMw35hSJeJqoPTNV7tFgTuw4q+vk1Qszx0c3fVNUOwlRWOyauV8WV1EppS35Z5jl5Sqp+SraycvljhB+o7qwAzjWb1R8kOoMfytuEeJvSXydWc+LKSUwuBAGFrFvRR9V02SKmjr47e3JuCDuFIcRNEyUcnDfwpdC74nSKigNiWXcwHmr1yT9VFFVipjMUn1gfuFKpaoRWpn7EfT5Cz5c+mqQ/q07hWNeRLTMqITuBkCOhU+uXT8fX2nq9bKkXXVRpVf8Q0sftI3mO6swclGzKtKKH4roddADk7PFA0o7XJ7X4qDJUhl/wD4Ao//ABBjXfW391stHZFvNEypgfE/k4W/Cz9FK+kqzDJtxYlXFFVsm6/jyoevUZ2qmDlYPt/BR5/5Q/1Yh/DdOacuqrtPqfegF+Y2KlB+KGYyTkG8v904PUZrkTMYrDEgPTmnJQhOO6PFLkgyl1SI01bmPok3/B6oLqkMjuTbyrzVKX4ukewfWOJh8rH1jj7H67qnM0vXWTVoxslREJGPwDtxtcoc8opvbkjlc+/1xvtcfghV1JXyU9iDdl92oc02cr5ByJ5K30mOK/L1rYUVQJogQeYQdQL2SRlgJy227qp0SqLJTGTsdwtI0hzhf9FHqZcdnHWzQqGhLrPm/ZXMQDW2AsorHI7HIQeraOUN4RGlccEybPeo9L/4hQkR/wBRvE1eePYWOLXggg2IPRevPash6s0W/wDztMzcfW0Dn5Tcdfyk7532McujkkQkOSsi1Ppqb2q2SI/cLhbFhyasFpzyzUI3+bFbemkyao9e3Vfj/HahmTVS1LCyTMcwr2QhzVW1TEq8/HKOYPaq31LS8LJh02K6Hmmnv9h5qxmwraIs53GyM8up987Gf0msxaad/I7tVrFK+iq46mOxBG4PIrMSsfTzlh2LSrihqviYDG/+ozf8q/7HHmVH1v25al8kcfth3Nt7i6j0c+VJJCft3CPqMoc4MtvbdVWRikNuoxKWzxT4+s6Ppag01SyUdDYjuFrYpA+MPYbgi48rGtGTld6NVf8ATPPlh/0U+p46Yti5AqJsGkk2A5o7hwqn1Z5bGBvubJJNo9dZNT44coxUZ/MacmjoOybqmrfFuZHIxvuMbiXhtiVS0tVJTyXBNuRHQrknFI8i9ufchdGSOK9W1Npqp9NKHs77joVqYJY62m6EObYhYpr8m/hWmlVhp5RETs7ceVPvn+xf4u/5XbHT9QMRPATse46KyEmW91H16POBlQObTYqNR1GcdjzHNTs10p7pcEOR8jsN/rNhfkEE/NlZH3NlKpwJa6okmsKanNh5IC3PMpO+rJ4JLQmGCOZ8pPFZ4xARnQmLjglErG/UAbkKOyQajUs3NiNmtsQxu/PzyXX0UrqYS0r8KhnC4DYm3ZNeYnz8nWrOCQPjWd12hDJSQPlzfw5TqGs4sJOCUHjZa1j4UrUIhU0Tx1tk1LPKt/6jCM4bsPMbJwKJWx4SiQcnIJK6Jdji75y4kUsxilY8fab/AJWxglD42PHIi6wwditTok3u0LB/acVP5J5qvwde4u43qVG9QIwpcVlGOqpjCnlBY5FA7p0q45qBLGHNtbYqTZMegzz71LoLqKQ1UAvA43cB9qz4Gy9YniEsZjeAWOFiCsnP6Oyme6GctjJu0dgq89/9T649VNO/Cdj+zlsqV/ywe4WED1sNJk92hiP+KXpvjv8AFqTko8rUVhXHhKupquPmodLXmll9t/8ATPI9laVTcrhU9VEG3WjUXVaVlXH8RBb3ANwOqpqd/tTsJvYO4rbFEbUSRO+W8jx0Qnn3nFx2PgbKnNzyuf5Pj32JeoOifIHwkPuN7KtfxOCc4FvUJ0bOqa2YTji766xie27HAg2INweye0JWSV0xoaGpFVTA/eNnDyousUxdTZgfSbqFQVBppw/7Ds4K/kayWLoQQl/KFmzGOBTg8t5J9XCYZSzoDsggq0uxx9Sy4IHYuuP2R2uLo7jm03CiAo0D+Ky1gS40JqWVejPP3tFnDtZVUMvtSA/v+EKKd8XuxjlI21kwQyO6qV5yu3jr7crq+Mkcw5A3Kk6iS6iEUAF5ZANuvVQdNPyzC837KfBwuDDzYbtSzyj1NgUhj0mmEUZcZX7uc0bj8LjaWZrRMMvcaA8xg2JBvtfume8YnSVs+7C/gjJ7bBPjr6qVvxEZik4bYtF/Nvync/n4HSV8dXKI61hY8OsyQ7OHgq3iL23jfzbt+VXQ1FLqu08GEvLIbA/qrFkXsxsGZeAMbu3NknS/xX+M/W0wf7sX+XCeyoyCxxY/Yg2IWkn4qmT/ALlHqtMNT8yOwktY32BR56zyh8nH29iiWk9Ng/CSE8s7D9gqtui1rpLOYGDq4uBAWjpIWU0DIo+TRz6lHvqWeE+HizranNKkRFRGKXEFF1VKYSjtQIyjNTpU8lNccuicEiEQCI8IWIUhwTMUGeTBy1np5+WnsHY2WQH1LR+mpcYJGdnXVOp4jx+tQwp7hwoEb0UcTVNdAquFVVTA9zbkFaJ9Ox26jywjG1kTayMkD8tmFC9mRvMLSyUwy5KNLCOyIWazpYct0VgU+opvChluDrI6XMILoSATrLCQCs9NrMG+zIdvtJ6eFXNbdHZEhRStSoxK0kc+az72ljrFaKOoxbZ+4/lQtQhhmb7kbwH9Qdro89e4l8nGzVTdIFIjF1imqrmwdjvmM73U8uDVXQtL3XG9t1MY1/VT7/XR8UyDxSPbIHDoVcXzaJY+26pQ16mUNQ+F2D/oP8JFyr4paueOMbRBty7pddhqo6ZzIoGGUw/a3Yef1U2UcJMfUbKrpzFQuweSZyciQLkJpUOucup8ZpdRv7d4pL3e25B/OysGh8VIc5DIWi4cRYkKkp4GSu+K0+UCUG5YRYfhWlROfhNxg9wsW3vZL0p8f+okTRkSdyd1MZ9KgxO4lKjf0SVRJDTiusYkxOaEphGtR4wUJoR2IhUiNHaUFhRQPKeEooK7dNaF26JCch2KIVxYXj4+pW2gVAiqyw/cFUfciRvLJA8HcG6pfY55crdRyqZG5UlFUiVoeDzCtIHqX46Jdic0ZIcjMk+N2SLiiyvkixUWSLwrZ8SiyMWPFRPGq6eny5BXskOaC+ANbayws49pY6xT2HJWVTShzeSq5AYXWKaUlmJTLNSdN0CiRl8rrDYKwgijZz3K1ANsUsvgIgoOrzdTWAu5DZPczylMq5qCN/g9wop00ZbvJHYCytpIjzBQHB7U32pLxLdRmRMhbgwbfyU8FPJ7roActdHyONOSeAmhieGlKaHslezYHbseSFKC6USsjBk5El1rIuKWK34FmzKHTljJBNHG6GS1ntP0lEe98rtyuEYpALX1pM8gsbeJGjIa5R2FOYeJJTxYxvRo1Fiby3UxjTkgIzGozGobApDAjAojAiApgTwE0JTgU8FMATgiDq5cpFJYHjxXQkkqudP02s+HkweeA/wtRSzZNukkp9K8VZRPU2M5JJJVDi3JAfDn+F1JEYE+EYqO6LwkkgaIlRFi0qjq4s9rX7JJIwb+GNpZom3wUmkiLnXfz7JJIkWrIwieyHNSSQMDJDioz4v/AEpJICA6LHnsmYd/4SSTFpNafyitI6pJLAK1gXfaSSS0TXRoTmFqSSzEAixsySSWoxNpwrBreJJJIYZgRWtxXUkS08J4KSSaAdddBSSTFK6V0kkGf//Z',
              }}
              style={styles.imageAboutContainer}
            />
          </View>
          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri: 'https://i.redd.it/wchv33xu03121.jpg',
              }}
              style={styles.imageAboutContainer}
            />
          </View>
          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSg2iijK2TtGg_KA-crtk8za5nRe4qmBKQqxg&usqp=CAU',
              }}
              style={styles.imageAboutContainer}
            />
          </View>
          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri:
                  'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS1u_KDUChgOLclEYmAaPaKxvg0UjWi5FP4ug&usqp=CAU',
              }}
              style={styles.imageAboutContainer}
            />
          </View>
          <View style={styles.aboutMeContainer}>
            <Image
              source={{
                uri:
                  'https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/close-up-of-woman-fingers-with-nail-art-manicure-royalty-free-image-1569858141.jpg?crop=1.00xw:0.754xh;0,0.161xh&resize=480:*',
              }}
              style={styles.imageAboutContainer}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  aboutMeContainer: {
    width: '100%',
    height: 100,
    alignItems: 'center',
    marginTop: 14,
  },
  imageAboutContainer: {
    width: '100%',
    height: '100%',
    borderRadius: 30,
  },
});
