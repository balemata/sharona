import React from 'react';
import { View, Text, StyleSheet, Platform } from 'react-native';

import {
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

import SharonaImage from '../components/SharonaImage';
import EntryPageButton from '../components/EntryPageButton';

export default class EntryPage extends React.Component {
  constructor(props) {
    super(props);
  }

  createAccountButtonClicked = () => {
    this.props.navigation.navigate('SignUpPage');
  };

  loginClicked = () => {
    this.props.navigation.navigate('LoginPage');
  };

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.sharonaImageCon}>
          <SharonaImage />
        </View>
        <View style={styles.buttonCon}>
          <EntryPageButton
            createAccountButtonClicked={this.createAccountButtonClicked}
            title="משתמש חדש"
          />
        </View>
        <View style={styles.connectTextCon}>
          <Text>
            כבר יש לך חשבון?{' '}
            <Text onPress={this.loginClicked} style={styles.loginButton}>
              לחץ כאן
            </Text>
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10
    // borderColor: 'purple',
    // borderWidth: 10
  },
  sharonaImageCon: {
    marginTop: Platform.OS === 'android' ? 0 : hp('2.3%')
    // borderColor: 'black',
    // borderWidth: 10
  },
  buttonCon: {
    flex: 3,
    alignItems: 'center',
    justifyContent: 'center'
    // borderColor: 'blue',
    // borderWidth: 10
  },
  connectTextCon: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
    // borderColor: 'red',
    // borderWidth: 10
  },
  loginButton: {
    color: 'blue',
    textDecorationLine: 'underline'
  }
});
