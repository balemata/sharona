import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class NavigateContact extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.screen}>
        <View style={styles.aboutMeContainer}>
          <Image
            source={{
              uri:
                'https://assets.rebelmouse.io/eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpbWFnZSI6Imh0dHBzOi8vYXNzZXRzLnJibC5tcy82NDcyMTMwL29yaWdpbi5qcGciLCJleHBpcmVzX2F0IjoxNjE0NDYzOTI3fQ.iAFB1f8WjYM-i5fGABMhpGHKNryjgaouoOQU4Bj9WEk/img.jpg?width=980',
            }}
            style={styles.imageAboutContainer}
          />
        </View>
        <View style={styles.details}>
          <View style={styles.address}>
            <Text>דרך החשמונאים 44, נתניה</Text>
            <TouchableOpacity style={styles.btnAboutMe}>
              <Text>ניווט בעזרת Waze</Text>
            </TouchableOpacity>
            <View style={styles.followUs}>
              <Icon onPress={() => {}} size={26} name={'facebook'} />
              <Icon onPress={() => {}} size={26} name={'instagram'} />
              <Icon onPress={() => {}} size={26} name={'whatsapp'} />
              <Icon onPress={() => {}} size={26} name={'phone'} />
            </View>
          </View>
        </View>
        <Text style={{ textAlign: 'center' }}>
          {' '}
          Cמערכות לניהול עסקים | M{`&`}T
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  aboutMeContainer: {
    width: '100%',
    height: 200,
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  imageAboutContainer: {
    width: '100%',
    height: '90%',
    borderRadius: 30,
  },
  details: {
    flex: 1,
  },
  address: {
    alignItems: 'center',
  },
  btnAboutMe: {
    alignItems: 'center',
    borderRadius: 30,
    borderWidth: 2,
    borderColor: '#888',
    width: '35%',
    marginTop: 30,
    marginBottom: 50,
    backgroundColor: '#87ceeb',
  },
  followUs: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
});
