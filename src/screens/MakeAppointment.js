// import _ from 'lodash';
// import React, { Component } from 'react';
// import {
//   Platform,
//   Alert,
//   StyleSheet,
//   View,
//   Text,
//   TouchableOpacity,
//   Button
// } from 'react-native';

// import {
//   ExpandableCalendar,
//   CalendarList,
//   AgendaList,
//   CalendarProvider,
//   WeekCalendar
// } from 'react-native-calendars';

// import LinearGradient from 'react-native-linear-gradient';
// import Colors from '../../assets/colors/Colors';

// const testIDs = require('../testIDs');

// const today = new Date().toISOString().split('T')[0];
// const fastDate = getPastDate(0);
// const futureDates = getFutureDates(30);
// const dates = [fastDate, today].concat(futureDates);

// function getFutureDates(days) {
//   const array = [];
//   for (let index = 1; index <= days; index++) {
//     const date = new Date(Date.now() + 864e5 * index); // 864e5 == 86400000 == 24*60*60*1000
//     const dateString = date.toISOString().split('T')[0];
//     array.push(dateString);
//   }
//   return array;
// }

// function getPastDate(days) {
//   return new Date(Date.now() - 864e5 * days).toISOString().split('T')[0];
// }

// const ITEMS = [
//   {
//     title: dates[1],
//     data: [
//       {
//         hour: '12am',
//         duration: '1h',
//         title: 'Ashtanga Yoga',
//         forWhat: 'Lak Gel'
//       }
//     ]
//   },
//   {
//     title: dates[2],
//     data: [
//       {
//         hour: '12am',
//         duration: '1h',
//         title: 'Ashtanga Yoga',
//         forWhat: 'Lak Gel'
//       }
//     ]
//   }
// ];

// export default class ExpandableCalendarScreen extends Component {
//   onDateChanged = (date, updateSource) => {
//     console.log('date changed', date);
//     // fetch and set data for date + week ahead
//   };

//   onMonthChange = (month, updateSource) => {
//     console.log('month changed', month);
//   };

//   buttonPressed() {
//     Alert.alert('show more');
//   }

//   itemPressed(id) {
//     Alert.alert(id);
//   }

//   renderEmptyItem() {
//     return (
//       <View style={styles.emptyItem}>
//         <Text style={styles.emptyItemText}>This is an empty day item</Text>
//       </View>
//     );
//   }

//   renderItem = ({ item }) => {
//     if (_.isEmpty(item)) {
//       return this.renderEmptyItem();
//     }

//     return (
//       <TouchableOpacity
//         onPress={() => this.itemPressed(item.title)}
//         style={styles.item}
//       >
//         <View>
//           <Text style={styles.itemHourText}>{item.hour}</Text>
//           <Text style={styles.itemDurationText}>{item.duration}</Text>
//         </View>
//         <Text style={styles.itemTitleText}>{item.title}</Text>
//         <View style={styles.itemButtonContainer}>
//           <Button color={'grey'} title={'Info'} onPress={this.buttonPressed} />
//         </View>
//       </TouchableOpacity>
//     );
//   };

//   getMarkedDates = () => {
//     const marked = {};
//     ITEMS.forEach(item => {
//       // NOTE: only mark dates with data
//       if (item.data) {
//         marked[item.title] = { marked: true };
//       }
//     });
//     return marked;
//   };

//   getTheme = () => {
//     return {
//       // arrows
//       arrowColor: Colors.secondary,
//       arrowStyle: { padding: 0 },
//       // month
//       monthTextColor: Colors.secondary,
//       textMonthFontSize: 20,
//       textMonthFontFamily: 'HelveticaNeue',
//       textMonthFontWeight: 'bold',
//       // day names
//       textSectionTitleColor: Colors.third,
//       textDayHeaderFontSize: 12,
//       textDayHeaderFontFamily: 'HelveticaNeue',
//       textDayHeaderFontWeight: 'normal',
//       // dates
//       dayTextColor: Colors.secondary,
//       textDayFontSize: 16,
//       textDayFontFamily: 'HelveticaNeue',
//       textDayFontWeight: '400',
//       textDayStyle: { marginTop: Platform.OS === 'android' ? 2 : 4 },
//       // selected date
//       selectedDayBackgroundColor: Colors.secondary,
//       selectedDayTextColor: 'white',
//       // disabled date
//       textDisabledColor: 'lightgrey',
//       // dot (marked date)
//       dotColor: 'black',
//       selectedDotColor: 'black',
//       disabledDotColor: 'lightgrey',
//       dotStyle: { marginTop: -2 }
//     };
//   };

//   render() {
//     return (
//       <CalendarProvider
//         date={new Date()}
//         onDateChanged={this.onDateChanged}
//         onMonthChange={this.onMonthChange}
//         showTodayButton
//         onDayPress={() => console.log('Matannnn')}
//         disabledOpacity={0.6}
//         markingType={'period'}
//         theme={{
//           todayButtonTextColor: Colors.third
//         }}
//         style={{ backgroundColor: 'white' }}
//         todayBottomMargin={16}
//       >
//         <LinearGradient
//           start={{ x: 0.5, y: 0.25 }}
//           end={{ x: 0.5, y: 0.9 }}
//           colors={[Colors.primary, Colors.secondary]}
//           style={styles.calendarProvider}
//         >
//           {/* <CalendarList
//             // Callback which gets executed when visible months change in scroll view. Default = undefined
//             onVisibleMonthsChange={months => {
//               console.log('now these months are visible', months);
//             }}
//             // Max amount of months allowed to scroll to the past. Default = 50
//             pastScrollRange={50}
//             // Max amount of months allowed to scroll to the future. Default = 50
//             futureScrollRange={50}
//             // Enable or disable scrolling of calendar list
//             scrollEnabled={true}
//             // Enable or disable vertical scroll indicator. Default = false
//             showScrollIndicator={true}
            
//           /> */}
//           {this.props.weekView ? (
//             <WeekCalendar
//               testID={testIDs.weekCalendar.CONTAINER}
//               // firstDay={0}
//               markedDates={this.getMarkedDates()}
//               onDayPress={() => console.log('Matan')}
//             />
//           ) : (
//             <ExpandableCalendar
//               testID={testIDs.expandableCalendar.CONTAINER}
//               onDayPress={() => console.log('Matan')}
//               // horizontal={true}
//               // disablePan
//               // hideKnob
//               // initialPosition={ExpandableCalendar.positions.OPEN}
//               // headerStyle={styles.calendar} // for horizontal only
//               // disableWeekScroll
//               // calendarStyle={styles.calendar}
//               // hideDayNames={true}
//               // showWeekNumbers={true}
//               // firstDay={0}
//               // hideArrows
//               onDayPress={() => console.log('Matan')}
//               theme={this.getTheme()}
//               minDate={today}
//               markedDates={this.getMarkedDates()} // {'2019-06-01': {marked: true}, '2019-06-02': {marked: true}, '2019-06-03': {marked: true}};
//               leftArrowImageSource={require('../../assets/images/calendar/previous.png')}
//               rightArrowImageSource={require('../../assets/images/calendar/next.png')}
//             />
//           )}
//           {/* <AgendaList
//             sections={ITEMS}
//             extraData={this.state}
//             renderItem={this.renderItem}
//             // sectionStyle={styles.section}
//           /> */}
//         </LinearGradient>
//       </CalendarProvider>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   calendarProvider: {
//     flex: 1,
//     marginTop: '24%'
//   },
//   calendar: {
//     paddingLeft: 10,
//     paddingRight: 10
//   },
//   section: {
//     backgroundColor: 'black',
//     color: 'green',
//     textTransform: 'capitalize'
//   },
//   item: {
//     padding: 20,
//     backgroundColor: 'white',
//     borderBottomWidth: 1,
//     borderBottomColor: 'lightgrey',
//     flexDirection: 'row'
//   },
//   itemHourText: {
//     color: 'green'
//   },
//   itemDurationText: {
//     color: 'blue',
//     fontSize: 14,
//     marginTop: 4,
//     marginLeft: 4
//   },
//   itemTitleText: {
//     color: 'black',
//     marginLeft: 16,
//     fontWeight: 'bold',
//     fontSize: 16
//   },
//   itemButtonContainer: {
//     flex: 1,
//     alignItems: 'flex-end'
//   },
//   emptyItem: {
//     backgroundColor: 'red',
//     paddingLeft: 20,
//     height: 100,
//     justifyContent: 'center',
//     borderBottomWidth: 1,
//     borderBottomColor: 'lightgrey'
//   },
//   emptyItemText: {
//     color: 'black',
//     fontSize: 20
//   }
// });
