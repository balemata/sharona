import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class AboutMe extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.screen}>
        <View style={styles.aboutMeContainer}>
          <Image
            source={{
              uri:
                'https://images.pexels.com/photos/3422099/pexels-photo-3422099.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            }}
            style={styles.imageAboutContainer}
          />
        </View>
        <View style={styles.details}>
          <View style={styles.title}>
            <Text style={styles.textTitle}>Sharon Balestra</Text>
          </View>
          <View style={styles.body}>
            <Text>
              כל גבה שונה מהשניה ולכל אחת מאפייני פנים שונים, המטרה שלי היא
              לקרוא את המאפיינים האלה ולהתאים את הגבה למבנה הפנים ולהעניק את
              הטיפול הטוב ביותר.
            </Text>
          </View>
          <View style={styles.body}>
            <Text>
              כל גבה שונה מהשניה ולכל אחת מאפייני פנים שונים, המטרה שלי היא
              לקרוא את המאפיינים האלה ולהתאים את הגבה למבנה הפנים ולהעניק את
              הטיפול הטוב ביותר.
            </Text>
          </View>
          <Text style={{ textAlign: 'right', marginTop: 30, paddingHorizontal: 40 }}>באהבה, שרון</Text>
        </View>
        <Text style={{ textAlign: 'center' }}>
          {' '}
          Cמערכות לניהול עסקים | M{`&`}T
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  aboutMeContainer: {
    width: '100%',
    height: 200,
    alignItems: 'center',
    marginTop: 30,
    marginBottom: 30,
  },
  imageAboutContainer: {
    width: '100%',
    height: '90%',
    borderRadius: 30,
  },
  details: {
    flex: 1,
  },
  textTitle: {
    fontSize: 26,
  },
  body: {
    alignItems: 'flex-start',
    marginTop: 30,
    paddingHorizontal: 40
  },
  title: {
    alignItems: 'center',
  },
});
