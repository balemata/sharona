import React from 'react';
import {
  View,
  StyleSheet,
  Platform,
  ScrollView,
  Keyboard,
  ActivityIndicator
} from 'react-native';

import Colors from '../../assets/colors/Colors';
import PoweredByText from '../components/PoweredByText';
import SharonaImage from '../components/SharonaImage';
import MyForm from '../components/form/MyForm';

import * as UserServerActions from '../../server/fetch-users/users';

import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboardIsActive: false,
      loginIsLoading: false,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ keyboardIsActive: true });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardIsActive: false });
  };

  changeUserStatus = () => {
    this.props.navigation.navigate('SignUpPage');
  };

  onLoginSubmit = async (phoneNumber, password) => {
    const result = await UserServerActions.loginUser(phoneNumber, password);
    if(!result) return;

    this.setState({ loginIsLoading: true });
    setTimeout(() => {
      if(result.error){
        alert(result.message);
      } else {
          this.props.navigation.navigate('UserDrawerNavigator');
      }
      this.setState({ loginIsLoading: false });
    },2000);
  };

  render() {
    const scrollEnabled = this.state.keyboardIsActive;

    if(this.state.loginIsLoading){
      return(
      <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator size="large" color={Colors.secondary} />
      </View>)
    }

    return (
      <View style={styles.containerTop}>
        <ScrollView
          scrollEnabled={scrollEnabled}
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <View style={styles.sharonaImage}>
            <SharonaImage />
          </View>

          <View style={styles.form}>
            <MyForm
              onTextChange={this.onTextChange}
              onLoginSubmit={this.onLoginSubmit}
              newUser={false}
              signUpIsLoading={this.state.signUpIsLoading}
              loginIsLoading={this.state.loginIsLoading}
              changeUserStatus={this.changeUserStatus}
            />
          </View>
          <View style={styles.footer}>
            <PoweredByText />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerTop: {
    flex: 1,
    backgroundColor: 'white'
  },
  sharonaImage: {
    marginTop: hp('5%')
  },
  form: {
    flex: 1,
  },
  text: {
    fontSize: hp('2%'),
    fontWeight: 'bold'
  },
  footer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: Platform.OS === 'android' ? hp('0.1%') : hp('2.8%')
  }
});
