import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import CalendarPicker from 'react-native-calendar-picker';
import Schedule from '../components/Schedule';

export default class BookAppointment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: null,
    };
    this.onDateChange = this.onDateChange.bind(this);
  }

  onDateChange(date) {
    this.setState({
      selectedStartDate: date,
    });
  }

  render() {
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';

    const customDatesStylesCallback = (date) => {
      switch (date.isoWeekday()) {
        case 6:
          return {
            style: {
              backgroundColor: 'red',
            },
          };
      }
    };

    return (
      <View style={styles.screen}>
        <View style={styles.aboutMeContainer}>
          <Image
            source={{
              uri:
                'https://images.pexels.com/photos/1059383/pexels-photo-1059383.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
            }}
            style={styles.imageAboutContainer}
          />
        </View>
        <View style={styles.calendar}>
          <CalendarPicker
            customDatesStyles={customDatesStylesCallback}
            selectedDayColor="pink"
            width={400}
            previousTitle="קודם"
            nextTitle="הבא"
            weekdays={[
              'ראשון',
              'שני',
              'שלישי',
              'רביעי',
              'חמישי',
              'שישי',
              'שבת',
            ]}
            months={[
              'ינואר',
              'פבואר',
              'מרץ',
              'אפריל',
              'מאי',
              'יוני',
              'יולי',
              'אוגוסט',
              'ספטמבר',
              'אוקטובר',
              'נובמבר',
              'דצמבר',
            ]}
            onDateChange={this.onDateChange}
          />
          {startDate.substring(0, 3) == 'Sat' && (
            <Text
              style={{
                fontSize: 26,
                textAlign: 'center',
                marginTop: 60,
              }}
            >
              שבת שלום
            </Text>
          )}
          {startDate.substring(0, 3) != 'Sat' && <Schedule />}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
  aboutMeContainer: {
    width: '100%',
    height: 100,
    alignItems: 'center',
    marginTop: 10,
  },
  imageAboutContainer: {
    width: '100%',
    height: '100%',
    borderRadius: 30,
  },
  calendar: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});
