// SignUp.js
import React from 'react';
import {
  View,
  ScrollView,
  ActivityIndicator,
  StyleSheet,
  Platform,
  Keyboard
} from 'react-native';

import Colors from '../../assets/colors/Colors';
import * as UserServerActions from '../../server/fetch-users/users';

import md5 from 'md5';
import PoweredByText from '../components/PoweredByText';
import SharonaImage from '../components/SharonaImage';
import MyForm from '../components/form/MyForm';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from 'react-native-responsive-screen';

export default class SignUpPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      keyboardIsActive: false,
      signUpIsLoading: false,
    };
  }

  changeUserStatus = () => {
    this.props.navigation.navigate('LoginPage');
  };

  onSignUpSubmit = async (name, pass, pn) => {
    hpass = md5(pass);
    const result = await UserServerActions.createNewUser(name, hpass, pn);
    if(!result) return;

    this.setState({ signUpIsLoading: true });
    setTimeout(() => {
      if(result.error){
          alert(result.message);
      } else {
          this.props.navigation.navigate('UserDrawerNavigator');
      }
      this.setState({ signUpIsLoading: false });
    },1500);
  };

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow = () => {
    this.setState({ keyboardIsActive: true });
  };

  _keyboardDidHide = () => {
    this.setState({ keyboardIsActive: false });
  };

  render() {
    const scrollEnabled = this.state.keyboardIsActive;

    if(this.state.signUpIsLoading){
      return(
      <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
        <ActivityIndicator size="large" color={Colors.secondary} />
      </View>)
    }
    
    else return (
      <View style={styles.containerTop}>
        <ScrollView
          scrollEnabled={scrollEnabled}
          contentContainerStyle={{ flexGrow: 1 }}
        >
          <View style={styles.sharonaImage}>
            <SharonaImage />
          </View>

          <View style={styles.form}>
            <MyForm
              onTextChange={this.onTextChange}
              onSignUpSubmit={this.onSignUpSubmit}
              newUser={true}
              signUpIsLoading={this.state.signUpIsLoading}
              changeUserStatus={this.changeUserStatus}
            />
          </View>

          <View style={styles.footer}>
            <PoweredByText />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerTop: {
    flex: 1,
    backgroundColor: 'white'
    // alignItems: 'center'
    // borderColor: 'red',
    // borderWidth: 10
  },
  sharonaImage: {
    marginTop: hp('5%')
    // borderColor: 'blue',
    // borderWidth: 10
  },
  form: {
    flex: 1
    // marginTop: hp('3.5%')
    // borderColor: 'green',
    // borderWidth: 10
  },
  text: {
    fontSize: hp('2%'),
    fontWeight: 'bold'
  },
  footer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginBottom: Platform.OS === 'android' ? hp('0.1%') : hp('3%')
    // borderColor: 'purple',
    // borderWidth: 10
  }
});
