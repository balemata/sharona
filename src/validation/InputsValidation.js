module.exports = {
  validatePhoneNumber(pn) {
    // Phone Number Validation Regex
    const regex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

    if (!regex.test(pn)) {
      return { valid: false, error: 'מספר טלפון לא תואם לתבנית' };
    } else {
      return { valid: true, error: '' };
    }
  },

  validatePassword(pass) {
    if (pass.length < 4) {
      return { valid: false, error: 'סיסמא חייבת להכיל לפחות 4 תוים' };
    } else {
      return { valid: true, error: '' };
    }
  },
  validateName(n) {
    if (n.length <= 0) {
      return { valid: false, error: 'שדה שם חייב להיות מלא' };
    } else {
      return { valid: true};
    }
  }
};
